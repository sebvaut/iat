# iat

iat (it's about time) is a desktop time management application. It allows you to create tasks, then create daily worklogs that actively track the tasks.

## Project Status
The project is currently in v0.1.0 alpha status.

### Project Milestones

See the [Milestones](https://gitlab.com/sebvaut/iat/-/milestones?sort=name_asc) for future planned releases

## Technologies Used

Iat is a desktop app created using the [Wails](https://wails.app/about/) framework. The backend is written in Go and the frontend is written in Vue (Vuteify + Webpack).

## Build Guide
Follow the [Wails getting started guide](https://wails.app/gettingstarted/).

### Launching the app for development

1. Launch the backend
    ```
    wails serve
    ```

2. Launch the frontend (in another terminal)
    ```
    cd frontend/
    npm run serve
    ```

### Building the app
The binary can be built using the wails build command

```
wails build
```