package server

import (
	"time"
)

// WorkLog is a specific work log for a task from a specific day
type WorkLog struct {
	TaskID   string
	Date     time.Time
	Duration time.Duration
}

// DurationString returns the rounded duration of a task
func (w WorkLog) DurationString() string {
	return w.Duration.Round(time.Second).String()
}

// GetWorkLogs returns all work logs
func (s *Server) GetWorkLogs() []WorkLog {
	s.lck.RLock()
	defer s.lck.RUnlock()
	w := make([]WorkLog, len(s.worklogs))
	for i, v := range s.worklogs {
		w[i] = v
	}
	return w
}

// GetTaskWorkLogs returns all worklogs for a specific task
func (s *Server) GetTaskWorkLogs(taskID string) []WorkLog {
	s.lck.RLock()
	defer s.lck.RUnlock()
	return s.getTaskWorkLogs(taskID)
}

// GetTaskWorkLog returns a worklog for a specific task ID and date
func (s *Server) GetTaskWorkLog(taskID string, date time.Time) WorkLog {
	s.lck.RLock()
	defer s.lck.RUnlock()
	return s.getTaskWorkLog(taskID, date)
}

// GetTaskWorkLoggedToday returns the duration worked today for a specific task
// this includes the combination of the current active worklog
func (s *Server) GetTaskWorkLoggedToday(taskID string) string {
	s.lck.RLock()
	defer s.lck.RUnlock()
	if s.isActiveWorklogTask(taskID) {

		return s.activeWorklog.DurationString()
	}
	return s.getTaskWorkLog(taskID, time.Now()).DurationString()
}

// SetTaskWorkLog updates or creates a new worklog
func (s *Server) SetTaskWorkLog(worklog WorkLog) {
	s.lck.Lock()
	defer s.lck.Unlock()
	s.setTaskWorkLog(worklog)
}

// StartTaskWorkLog starts a worklog for a specific task at the current date
func (s *Server) StartTaskWorkLog(taskID string) {
	s.lck.Lock()
	defer s.lck.Unlock()
	// stop any existing worklog first
	s.stopCurrentTaskWorkLog()

	wl := s.getTaskWorkLog(taskID, time.Now())
	wl.Date = time.Now()

	s.activeWorklog = &wl
	s.rt.Events.Emit("worklog_active_"+wl.TaskID, true)
}

// StopCurrentTaskWorkLog stops the current active worklog
func (s *Server) StopCurrentTaskWorkLog() {
	s.lck.Lock()
	defer s.lck.Unlock()
	s.stopCurrentTaskWorkLog()
}

// StopIfStartedTaskWorkLog stops a worklog by a specific task ID
// only if there's an active worklog for that task
func (s *Server) StopIfStartedTaskWorkLog(taskid string) {
	s.lck.Lock()
	defer s.lck.Unlock()
	if s.activeWorklog == nil {
		return
	}
	if s.activeWorklog.TaskID != taskid {
		return
	}
	s.stopCurrentTaskWorkLog()
}

// GetCurrentTaskWorkLog returns the current active worklog
// TaskID will be an empty string if there's no current active worklog
func (s *Server) GetCurrentTaskWorkLog() WorkLog {
	s.lck.RLock()
	defer s.lck.RUnlock()

	if s.activeWorklog == nil {
		return WorkLog{
			TaskID: "",
		}
	}
	t := time.Now().Sub(s.activeWorklog.Date) + s.activeWorklog.Duration
	return WorkLog{
		TaskID:   s.activeWorklog.TaskID,
		Date:     s.activeWorklog.Date,
		Duration: t,
	}

}

func (s *Server) getTaskWorkLogs(taskID string) []WorkLog {

	worklogs := make([]WorkLog, 0)
	for _, w := range s.worklogs {
		if w.TaskID == taskID {
			worklogs = append(worklogs, w)
		}
	}
	return worklogs
}

func (s *Server) getTaskWorkLog(taskID string, date time.Time) WorkLog {

	for _, w := range s.worklogs {

		if w.TaskID == taskID && s.sameDate(w.Date, date) {
			return w
		}
	}
	return WorkLog{
		TaskID:   taskID,
		Date:     date,
		Duration: 0,
	}
}

func (s *Server) isActiveWorklogTask(taskID string) bool {

	if s.activeWorklog != nil && s.activeWorklog.TaskID == taskID {
		return true
	}
	return false
}

func (s *Server) setTaskWorkLog(worklog WorkLog) {
	for i, w := range s.worklogs {
		if w.TaskID == worklog.TaskID && s.sameDate(w.Date, worklog.Date) {
			s.worklogs[i] = worklog
			return
		}
	}
	s.worklogs = append(s.worklogs, worklog)
}

func (s *Server) stopCurrentTaskWorkLog() {
	if s.activeWorklog == nil {
		return
	}
	elapsed := time.Now().Sub(s.activeWorklog.Date)
	wl := s.activeWorklog
	wl.Duration += elapsed
	s.activeWorklog = nil
	s.setTaskWorkLog(*wl)

	s.rt.Events.Emit("worklog_active_"+wl.TaskID, false)

}
