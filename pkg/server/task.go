package server

// Task defines a single task
type Task struct {
	ID     string
	Name   string
	Type   string
	Ticket string
	Done   bool
}

// GetTasks returns all tasks
func (s *Server) GetTasks() []Task {
	s.lck.RLock()
	defer s.lck.RUnlock()

	return s.getTasks()
}

// GetTask returns a specific task by task ID
func (s *Server) GetTask(id string) Task {
	s.lck.RLock()
	defer s.lck.RUnlock()
	for _, t := range s.tasks {
		if t.ID == id {
			return t
		}
	}
	return Task{
		Name: "Not found",
		ID:   "n/a",
	}
}

// NewTask creates a new task
func (s *Server) NewTask(Name, Type, Ticket string) (TaskId string) {
	s.lck.Lock()
	defer s.lck.Unlock()
	id := s.newID()
	s.tasks = append(s.tasks, Task{
		ID:     id,
		Name:   Name,
		Type:   Type,
		Ticket: Ticket,
		Done:   false,
	})

	// refresh frontend task list
	s.sendUpdateTaskEvent()

	return id
}

// UpdateTask updates an existing task
func (s *Server) UpdateTask(TaskID, Name, Type, Ticket string) {
	s.lck.Lock()
	defer s.lck.Unlock()
	i, _ := s.getTask(TaskID)
	if i == -1 {
		return
	}
	s.tasks[i].Name = Name
	s.tasks[i].Type = Type
	s.tasks[i].Ticket = Ticket

	// refresh frontend task list
	s.sendUpdateTaskEvent()

}

func (s *Server) getTasks() []Task {

	t := make([]Task, len(s.tasks))
	for i, v := range s.tasks {
		t[i] = v
	}
	return t
}

func (s *Server) getTask(id string) (int, Task) {
	for i, t := range s.tasks {
		if t.ID == id {
			return i, t
		}
	}
	return -1, Task{
		Name: "Not found",
		ID:   "n/a",
	}
}

func (s *Server) sendUpdateTaskEvent() {
	if s.rt == nil {
		return
	}
	s.rt.Events.Emit("update_task_list", s.getTasks())
}
