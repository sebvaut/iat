package server

import (
	"fmt"
	"log"
	"sync"
	"time"

	"github.com/sony/sonyflake"
	"github.com/wailsapp/wails"
)

// NewServer creates a new server instance
func NewServer() *Server {
	return &Server{
		tasks:    make([]Task, 0),
		worklogs: make([]WorkLog, 0),
		idGen:    sonyflake.NewSonyflake(sonyflake.Settings{}),
		lck:      &sync.RWMutex{},
	}
}

// Server contains all the backend logic and state of the application
type Server struct {
	tasks         []Task
	worklogs      []WorkLog
	activeWorklog *WorkLog
	idGen         *sonyflake.Sonyflake
	lck           *sync.RWMutex
	rt            *wails.Runtime
}

// WailsInit is used to init the Wails runtime
func (s *Server) WailsInit(runtime *wails.Runtime) error {
	s.rt = runtime
	go s.activeWorklogEvents()
	return nil
}

// activeWorklogEvents emits events to update the active worklog's time
func (s *Server) activeWorklogEvents() {
	windowTitleNoTask := false
	for {
		wl := s.GetCurrentTaskWorkLog()
		if wl.TaskID != "" {
			d := wl.Duration.Round(time.Second).String()
			task := s.GetTask(wl.TaskID)
			s.rt.Events.Emit("worklog_update_"+wl.TaskID, d)

			s.rt.Window.SetTitle("iat - " + task.Name + " - " + d)
			windowTitleNoTask = false

		} else if windowTitleNoTask == false {
			s.rt.Window.SetTitle("ait - no tasks started")
			windowTitleNoTask = true
		}
		time.Sleep(time.Second)
	}
}

func (s *Server) newID() string {
	id, err := s.idGen.NextID()
	if err != nil {
		log.Fatalln(err)
	}
	return fmt.Sprintf("%x", id)
}

func (s *Server) sameDate(x time.Time, y time.Time) bool {
	xy, xm, xd := x.Date()
	yy, ym, yd := y.Date()
	if xy == yy &&
		xm == ym &&
		xd == yd {
		return true
	}
	return false
}
