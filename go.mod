module iat

require (
	github.com/leaanthony/mewn v0.10.7
	github.com/sony/sonyflake v1.0.0
	github.com/wailsapp/wails v1.0.1
)

go 1.13
