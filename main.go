package main

import (
	"iat/pkg/server"

	"github.com/leaanthony/mewn"
	"github.com/wailsapp/wails"
)

func basic() string {
	return "Hello World!"
}

func main() {
	svr := server.NewServer()

	// Test tasks
	svr.NewTask("Task 1", "Service Now", "1234567")
	svr.NewTask("Task 2", "Jira", "29304")
	svr.NewTask("Task 3", "Jira", "29304")
	svr.NewTask("Task 4", "Jira", "29304")

	runApp(svr)
}

func runApp(svr *server.Server) {
	js := mewn.String("./frontend/dist/app.js")
	css := mewn.String("./frontend/dist/app.css")

	app := wails.CreateApp(&wails.AppConfig{
		Width:  1024,
		Height: 768,
		Title:  "iat",
		JS:     js,
		CSS:    css,
		Colour: "#131313",
	})
	app.Bind(svr)
	app.Run()
}
